#!/usr/bin/env python3

import datetime
import dataclasses

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox import options
from selenium.webdriver.support.ui import Select

from credentials import PASSWORD, USERNAME

# establish driver
opts = options.Options()
opts.add_argument("--headless")
driver = webdriver.Firefox(options=opts)
driver.get("https://www.pollendiary.com/Phd/de/start")

# log in
driver.find_element(By.ID, "email").send_keys(USERNAME)
driver.find_element(By.ID, "password").send_keys(PASSWORD)
driver.find_element(By.ID, "loginButton").click()

# we should be at datenerfassung
assert "Datenerfassung" in driver.title


# switch to certain data
diary_date = datetime.datetime(year=2022, month=3, day=8)
diary_date.strftime("%Y-%m-%d")
driver.get(
    f"https://www.pollendiary.com/Phd/de/diary.datebox.newdate/{diary_date:%Y-%m-%d}"
)
driver.get_full_page_screenshot_as_file("test0.png")


# prepare the datenerfassung inputs
class ProblemsScale:
    NONE = "0"
    WEAK = "1"
    MODERATE = "2"
    STRONG = "3"


class OverallSymptoms:
    VERY_GOOD = "1"
    GOOD = "3"
    MEDIUM = "5"
    POOR = "7"
    VERY_POOR = "9"


@dataclasses.dataclass
class EyeSymptoms:
    eyeItching: bool = False
    eyeRedness: bool = False
    eyeForeignBody: bool = False
    eyeWatering: bool = False


@dataclasses.dataclass
class NoseSymptoms:
    noseItching: bool = False
    noseRunning: bool = False
    noseSneezing: bool = False
    noseBlocked: bool = False


@dataclasses.dataclass
class LungsSymptoms:
    lungsWheezing: bool = False
    lungsCough: bool = False
    lungsShortnessOfBreath: bool = False
    lungsAsthma: bool = False


@dataclasses.dataclass
class Medis:
    medNone: bool = dataclasses.field(init=False)
    medAntiAllergicTablet: bool = False
    medCortisoneTablet: bool = False
    medNoseOrEyeDropsWoCortisone: bool = False
    medCortisoneInNoseOrEye: bool = False
    medAsthmaSprayWoCortisone: bool = False
    medAsthmaSprayWCortisone: bool = False
    medLeukotrieneReceptorAntagonist: bool = False
    noseSprayWCortisoneAndAntiHistaminicum: bool = False
    medMuscarineReceptorAntagonist: bool = False
    medImmunotherapy: bool = False
    medNaturalMedicine: bool = False

    def __post_init__(self):
        self.medNone = not any(
            [
                getattr(self, field.name)
                for field in dataclasses.fields(self)
                if field.name != "medNone"
            ]
        )


@dataclasses.dataclass
class StrongSymptomsTime:
    ssm: bool = False
    ssd: bool = False
    sse: bool = False
    ssn: bool = False


@dataclasses.dataclass
class DiaryEntryData:
    place: str
    country: str = "DE"
    overall_symptoms: OverallSymptoms = OverallSymptoms.VERY_GOOD
    eye_problems: ProblemsScale = ProblemsScale.NONE
    eye_symptoms: EyeSymptoms = EyeSymptoms()
    nose_problems: ProblemsScale = ProblemsScale.NONE
    nose_symptoms: NoseSymptoms = NoseSymptoms()
    lungs_problems: ProblemsScale = ProblemsScale.NONE
    lungs_symptoms: LungsSymptoms = LungsSymptoms()
    medication: Medis = Medis()
    strong_symptoms_time: StrongSymptomsTime = StrongSymptomsTime()
    effect_on_fitness_for_work: ProblemsScale = ProblemsScale.NONE
    comments: str = ""

    @property
    def diary_entries_with_scale(self):
        return {
            "overallSymptoms": self.overall_symptoms,
            "eyeProblems": self.eye_problems,
            "noseProblems": self.nose_problems,
            "lungsProblems": self.lungs_problems,
            "effectOnFitnessForWork": self.effect_on_fitness_for_work,
        }

    @property
    def diary_entries_with_toggles(self):
        yield from [
            self.eye_symptoms,
            self.nose_symptoms,
            self.lungs_symptoms,
            self.medication,
            self.strong_symptoms_time,
        ]


no_problems_at_all = DiaryEntryData(place="88131 Lindau (Bodensee)")


# do the datenerfassung
place_elem = driver.find_element(By.ID, "place")
if not place_elem.is_enabled():
    driver.find_element(By.ID, "changeButton").click()
    place_elem = driver.find_element(By.ID, "place")

place_elem.clear()
place_elem.send_keys(no_problems_at_all.place)

Select(driver.find_element(By.ID, "country")).select_by_value(
    no_problems_at_all.country
)

for problem, scale in no_problems_at_all.diary_entries_with_scale.items():
    problem_scale_buttons = driver.find_elements(By.NAME, problem)
    for button in problem_scale_buttons:
        if button.get_attribute("value") == scale:
            button.click()

for toggle_entries in no_problems_at_all.diary_entries_with_toggles:
    for toggle_name, enable_toggle in dataclasses.asdict(toggle_entries).items():
        toggle = driver.find_element(By.ID, toggle_name)
        if (enable_toggle and not toggle.is_selected()) or (
            not enable_toggle and toggle.is_selected()
        ):
            driver.find_element(By.ID, toggle_name).click()


driver.get_full_page_screenshot_as_file("test1.png")

# and save
driver.find_element(By.ID, "saveButton").click()
driver.get_full_page_screenshot_as_file("test2.png")
driver.close()
